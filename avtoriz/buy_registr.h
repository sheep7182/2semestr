#ifndef BUY_REGISTR_H
#define BUY_REGISTR_H

#include <QDialog>

namespace Ui {
class buy_registr;
}

class buy_registr : public QDialog
{
    Q_OBJECT

public:
    explicit buy_registr(QWidget *parent = nullptr);
    ~buy_registr();

private slots:
    void on_pushButton_clicked();

private:
    Ui::buy_registr *ui;
};

#endif // BUY_REGISTR_H
