#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <QSqlTableModel>
#include <QDataWidgetMapper>
#include <QMessageBox>

#include <QMainWindow>
#include <database.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
explicit MainWindow(QWidget *parent = nullptr);
~MainWindow();

private slots:
void on_pushButton_clicked();

void on_pushButton_2_clicked();

private:
Ui::MainWindow *ui;
QSqlTableModel *model;
QDataWidgetMapper *mapper;

private:
void setupModel();
void accept();
static bool createConnection();
};

#endif // MAINWINDOW_H

