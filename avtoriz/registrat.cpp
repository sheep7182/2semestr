#include "registrat.h"
#include "ui_registrat.h"

registrat::registrat(int row, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registrat)
{
    ui->setupUi(this);

    /* Метода для инициализации модели, из которой будут транслироваться данные */
        setupModel();

        /* Если строка не задана, то есть равна -1,
         * тогда диалог работает по принципу создания новой записи.
         * А именно, в модель вставляется новая строка и работа ведётся с ней.*/
        if(row == -1){
            model->insertRow(model->rowCount(QModelIndex()));
            mapper->toLast();
        /* В противном случае диалог настраивается на заданную запись
         * */
        } else {
            mapper->setCurrentModelIndex(model->index(row,0));
        };
}

registrat::~registrat()
{
    delete ui;
}

/* Метод настройки модели данных и mapper
 * */
void registrat::setupModel()
{
    /* Инициализируем модель и делаем выборку из неё
     * */
    model = new QSqlTableModel(this);
    model->setTable(USERS);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();

    /* Инициализируем mapper и привязываем
     * поля данных к объектам LineEdit
     * */
    mapper = new QDataWidgetMapper();
    mapper->setModel(model);
    mapper->addMapping(ui->lineEdit, 1);
    mapper->addMapping(ui->lineEdit_2, 2);
    mapper->addMapping(ui->lineEdit_3, 3);
    /* Ручное подтверждение изменения данных
     * через mapper
     * */
    mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    /* Подключаем коннекты от кнопок пролистывания
     * к прилистыванию модели данных в mapper
     * */
    connect(ui->pushButton, SIGNAL(clicked()), mapper, SLOT(toPrevious()));
    connect(ui->pushButton_2, SIGNAL(clicked()), mapper, SLOT(toNext()));
    /* При изменении индекса в mapper изменяем состояние кнопок
     * */
    connect(mapper, SIGNAL(currentIndexChanged(int)), this, SLOT(updateButtons(int)));
}


void registrat::on_buttonBox_accepted()
{
    /* SQL-запрос для проверки существования записи
     * с такими же учетными данными.
     * Если запись не существует или находится лишь индекс
     * редактируемой в данный момент записи,
     * то диалог позволяет вставку записи в таблицу данных
     * */
    QSqlQuery query;
    QString str = QString("SELECT EXISTS (SELECT " USERS_USERNAME " FROM " USERS
                          " WHERE ( " USERS_USERNAME " = '%1' "
                          " OR " USERS_STATUS " = '%2' )"
                          " AND id NOT LIKE '%3' )")
            .arg(ui->lineEdit->text(),
                 ui->lineEdit_3->text(),
                 model->data(model->index(mapper->currentIndex(),0), Qt::DisplayRole).toString());

    query.prepare(str);
    query.exec();
    query.next();

    /* Если запись существует, то диалог вызывает
     * предупредительное сообщение
     * */
    if(query.value(0) != 0){
        QMessageBox::critical(this,"Ошибка","Такой пользователь уже существует");
    /* В противном случае производится вставка новых данных в таблицу
     * и диалог завершается с передачей сигнала для обновления
     * таблицы в главном окне
     * */
    } else {
        mapper->submit();
        model->submitAll();
        emit signalReady();
        this->close();
    }
}

void registrat::accept()
{

}

/* Метод изменения состояния активности кнопок пролистывания
 * */
void registrat::updateButtons(int row)
{
    /* В том случае, если мы достигаем одного из крайних (самый первый или
     * самый последний) из индексов в таблице данных,
     * то мы изменяем состояние соответствующей кнопки на
     * состояние неактивна
     * */
    ui->pushButton->setEnabled(row > 0);
    ui->pushButton_2->setEnabled(row < model->rowCount() - 1);
}
