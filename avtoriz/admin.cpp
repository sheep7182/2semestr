#include "admin.h"
#include "ui_admin.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include "admin_newbooks.h"
#include "buy_registr.h"
#include "baza.h"

admin::admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin)
{
    ui->setupUi(this);
    QFile file("D://2semestr/avtoriz/baza.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {
            //читаем строку
            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }
}

admin::~admin()
{
    delete ui;
}

void admin::on_pushButton_clicked()
{
    hide();
    admin_newbooks New;
    New.setModal(true);
    New.exec();
}

void admin::on_pushButton_2_clicked()
{
    hide();
    buy_registr Buy;
    Buy.setModal(true);
    Buy.exec();
}

void admin::on_pushButton_3_clicked()
{
    hide();
    baza number1;
    number1.setModal(true);
    number1.exec();
}

