#ifndef BAZA_H
#define BAZA_H

#include <QDialog>
#include <QSqlTableModel>

#include <database.h>
#include <registrat.h>

namespace Ui {
class baza;
}

class baza : public QDialog
{
    Q_OBJECT

public:
    explicit baza(QWidget *parent = nullptr);
    ~baza();

private slots:
    void on_addUserButton_clicked();
    void slotUpdateModels();
    void slotEditRecord(QModelIndex index);

    void on_pushButton_2_clicked();

private:
    Ui::baza           *ui;
    DataBase           *db;
    QSqlTableModel     *modelUsers;

private:
    void setupModel(const QString &tableName, const QStringList &headers);
    void createUI();
};

#endif // BAZA_H
