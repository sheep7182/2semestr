#include "admin_newbooks.h"
#include "ui_admin_newbooks.h"
#include "books.cpp"
#include <QFile>
#include <QTextStream>
#include "admin.h"
#include <QMessageBox>
#include<QPixmap>


admin_newbooks::admin_newbooks(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin_newbooks)
{
    ui->setupUi(this);
    QPixmap pix(":/image/img/kniga_21-320.png");
    int w =ui->label_5->width();
    int h = ui->label_5->height();
    ui->label_5->setPixmap(pix.scaled(w,h, Qt::KeepAspectRatio));
}

admin_newbooks::~admin_newbooks()
{
    delete ui;
}

void admin_newbooks::on_pushButton_clicked()
{
    setlocale(LC_ALL,"Russian_Russia.1251");
    QString avtor= ui->lineEdit ->text();
    QString name = ui->lineEdit_2->text();
    QString price = ui->lineEdit_3->text();

    books new_books = books(avtor,name, price);

    new_books.add_in_db();

    QMessageBox::information(this, "Новая книга добавлена!", "Выполнено");
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");


    hide();
    admin again;
    again.setModal(true);
    again.exec();
}

