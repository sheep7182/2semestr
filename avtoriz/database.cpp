#include "database.h"

DataBase::DataBase(QObject *parent) : QObject(parent)
{

}

DataBase::~DataBase()
{

}

/* Методы для подключения к базе данных
 * */
void DataBase::connectToDataBase()
{
    /* Перед подключением к базе данных производим проверку на её существование.
     * В зависимости от результата производим открытие базы данных или её восстановление
     * */
    if(!QFile("D:/2semestr/avtoriz/avtiriz" DATABASE_NAME).exists()){
        this->restoreDataBase();
    } else {
        this->openDataBase();
    }
}

/* Методы восстановления базы данных
 * */
bool DataBase::restoreDataBase()
{
    if(this->openDataBase()){
        if(!this->createUsersTable()){
            return false;
        } else {
            return true;
        }
    } else {
        qDebug() << "Не удалось восстановить базу данных";
        return false;
    }
}

/* Метод для открытия базы данных
 * */
bool DataBase::openDataBase()
{
    /* База данных открывается по заданному пути
     * и имени базы данных, если она существует
     * */
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setHostName(DATABASE_HOSTNAME);
    db.setDatabaseName("D:/2semestr/avtoriz/avtoriz" DATABASE_NAME);
    if(db.open()){
        return true;
    } else {
        return false;
    }
}

/* Методы закрытия базы данных
 * */
void DataBase::closeDataBase()
{
    db.close();
}

/* Метод для создания таблицы устройств в базе данных
 * */
bool DataBase::createUsersTable()
{
    /* В данном случае используется формирование сырого SQL-запроса
     * с последующим его выполнением.
     * */
    QSqlQuery query;
    if(!query.exec( "CREATE TABLE " USERS " ("
                            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            USERS_USERNAME    " VARCHAR(255)    NOT NULL,"
                            USERS_PAROL          " VARCHAR(16)     NOT NULL,"
                            USERS_STATUS         " VARCHAR(5)     NOT NULL"
                        " )"
                    )){
        qDebug() << "DataBase: error of create " << USERS;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

/* Метод для вставки записи в таблицу устройств
 * */
bool DataBase::inserIntoUsersTable(const QVariantList &data)
{
    // Запрос SQL формируется из QVariantList, в который передаются данные для вставки в таблицу.

    QSqlQuery query;
    /* В начале SQL запрос формируется с ключами,
     * которые потом связываются методом bindValue
     * для подстановки данных из QVariantList
     * */
    query.prepare("INSERT INTO " USERS " ( " USERS_USERNAME   "; "
                                              USERS_PAROL "; "
                                              USERS_STATUS  " ) "
                  "VALUES (:Username; :Parol; :Status )");
    query.bindValue(":Username",    data[0].toString());
    query.bindValue(":Parol",          data[1].toString());
    query.bindValue(":Status",         data[2].toString());
    // После чего выполняется запросом методом exec()
    if(!query.exec()){
        qDebug() << "error insert into " << USERS;
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}
