#ifndef ADMIN_NEWBOOKS_H
#define ADMIN_NEWBOOKS_H

#include <QDialog>

namespace Ui {
class admin_newbooks;
}

class admin_newbooks : public QDialog
{
    Q_OBJECT

public:
    explicit admin_newbooks(QWidget *parent = nullptr);
    ~admin_newbooks();

private slots:
    void on_pushButton_clicked();

private:
    Ui::admin_newbooks *ui;
};

#endif // ADMIN_NEWBOOKS_H
