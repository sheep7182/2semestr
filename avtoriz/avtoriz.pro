#-------------------------------------------------
#
# Project created by QtCreator 2019-04-27T17:29:08
#
#-------------------------------------------------

QT       += core gui
QT += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = avtoriz
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        admin.cpp \
        admin_newbooks.cpp \
        baza.cpp \
        books.cpp \
        buy_registr.cpp \
        client.cpp \
        database.cpp \
        main.cpp \
        mainwindow.cpp \
        registrat.cpp

HEADERS += \
        admin.h \
        admin_newbooks.h \
        baza.h \
        books.h \
        buy_registr.h \
        client.h \
        database.h \
        mainwindow.h \
        registrat.h

FORMS += \
        admin.ui \
        admin_newbooks.ui \
        baza.ui \
        buy_registr.ui \
        client.ui \
        mainwindow.ui \
        registrat.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
