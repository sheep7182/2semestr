#include "client.h"
#include "ui_client.h"
#include <QFile>
#include <string>
#include<fstream>
#include <QString>
#include "QMessageBox"
#include <QTime>
#include <QDebug>
#include<QPixmap>

using namespace std;

client::client(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::client)
{
    ui->setupUi(this);
    QPixmap pix(":/image/img/skachat_knigu_prodvizhenie_rika_rennera_59532_2.png");
    int w =ui->image->width();
    int h = ui->image->height();
    ui->image->setPixmap(pix.scaled(w,h, Qt::KeepAspectRatio));



    QFile file("D://2semestr/avtoriz/baza.txt");
    if(file.open(QIODevice::ReadOnly |QIODevice::Text))
    {
        while(!file.atEnd())
        {

            QString str = file.readLine();
            ui->listWidget->addItem(str);
        }

    }

}


client::~client()
{
    delete ui;
}

void client::on_pushButton_clicked()
{
    /*QTime time;
    time.start();
    for(;time.elapsed() < 2000;);*/
    ui->label->setText(ui->listWidget->currentItem()->text());
    QString book_o=ui->label->text();
    string sbook=book_o.toUtf8().toStdString();
    ofstream file("D://2semestr//avtoriz//basket.txt", ofstream::app, ofstream::binary);
    file<<sbook;
    hide();
    QMessageBox::information(this,"Ура!","Книга добавлена в корзину!");
    client again;
    again.setModal(true);
    again.exec();
}

void client::on_pushButton_2_clicked()
{
    QMessageBox::StandardButton reply = QMessageBox::question(this,"Оповещение","Вы уверены,что хотите выйти?", QMessageBox::Yes | QMessageBox::No );

    if (reply==QMessageBox::Yes) {

        QApplication::quit();//закрытие диалогового окна
    }
    else {
        client again;
        again.setModal(true);
        again.exec();
    }
}
