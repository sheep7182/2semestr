#ifndef REGISTRAT_H
#define REGISTRAT_H

#include <QDialog>
#include <QSqlTableModel>
#include <QDataWidgetMapper>
#include <QMessageBox>

#include <database.h>

namespace Ui {
class registrat;
}

class registrat : public QDialog
{
    Q_OBJECT

public:
    explicit registrat(int row = -1, QWidget *parent = nullptr);
    ~registrat();

signals:
    void signalReady();

private slots:
    void on_buttonBox_accepted();
    void updateButtons(int row);

private:
    Ui::registrat         *ui;
    QSqlTableModel        *model;
    QDataWidgetMapper     *mapper;

   private:
       void setupModel();
       void accept();
};

#endif // REGISTRAT_H
