#include "baza.h"
#include "ui_baza.h"
#include "mainwindow.h"
#include"admin.h"

baza::baza(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::baza)
{
    ui->setupUi(this);
    this->setWindowTitle("QDataWidgetMapper Example");
        /* Первым делом необходимо создать объект для работы с базой данных
         * и инициализировать подключение к базе данных
         * */
        db = new DataBase();
        db->connectToDataBase();

        /* Инициализируем модели для представления данных
         * с заданием названий колонок
         * */
        this->setupModel(USERS,
                         QStringList() << trUtf8("id")
                                             << trUtf8("Логин")
                                             << trUtf8("Пароль")
                                             << trUtf8("Статус")
                   );
        /* Инициализируем внешний вид таблицы с данными
         * */
        this->createUI();
}

baza::~baza()
{
    delete ui;
}
void baza::setupModel(const QString &tableName, const QStringList &headers)
{
    /* Производим инициализацию модели представления данных
     * */
    modelUsers = new QSqlTableModel(this);
    modelUsers->setTable(tableName);
    modelUsers->select();
    /* Устанавливаем названия колонок в таблице с сортировкой данных
     * */
    for(int i = 0, j = 0; i < modelUsers->columnCount(); i++, j++){
        modelUsers->setHeaderData(i,Qt::Horizontal,headers[j]);
    }
}

void baza::createUI()
{
    ui->UserstableView->setModel(modelUsers);     // Устанавливаем модель на TableView
    ui->UserstableView->setColumnHidden(0, true);    // Скрываем колонку с id записей
    ui->UserstableView->setSelectionBehavior(QAbstractItemView::SelectRows); // Разрешаем выделение строк
    ui->UserstableView->setSelectionMode(QAbstractItemView::SingleSelection);   // Устанавливаем режим выделения лишь одной строки в таблице
    // Устанавливаем размер колонок по содержимому
    ui->UserstableView->resizeColumnsToContents();
    ui->UserstableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->UserstableView->horizontalHeader()->setStretchLastSection(true);

    connect(ui->UserstableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(slotEditRecord(QModelIndex)));
}

// Метод для активации диалога добавления записей
void baza::on_addUserButton_clicked()
{
    // Создаем диалог и подключаем его сигнал завершения работы к слоту обновления вида модели представления данных
    registrat *addUserDialog = new registrat();
    connect(addUserDialog, SIGNAL(signalReady()), this, SLOT(slotUpdateModels()));

    /* Выполняем запуск диалогового окна
     * */
    addUserDialog->setWindowTitle(trUtf8("Добавить Пользователя"));
    addUserDialog->exec();
}

/* Слот обновления модели представления данных
 * */
void baza::slotUpdateModels()
{
    modelUsers->select();
}

/* Метод для активации диалога добавления записей в режиме редактирования
 * с передачей индекса выбранной строки
 * */
void baza::slotEditRecord(QModelIndex index)
{
    /* Также создаем диалог и подключаем его сигнал завершения работы
     * к слоту обновления вида модели представления данных, но передаём
     * в качестве параметров строку записи
     * */
    registrat *addUserDialog = new registrat(index.row());
    connect(addUserDialog, SIGNAL(signalReady()), this, SLOT(slotUpdateModel()));

    /* Выполняем запуск диалогового окна
     * */
    addUserDialog->setWindowTitle(trUtf8("Редактировать Данные"));
    addUserDialog->exec();
}

void baza::on_pushButton_2_clicked(){
    admin ad1;
    ad1.setModal(true);
    ad1.exec();
}
